package com.themark.ak.app.android.weatherhelper

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_stub.*

class StubActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stub)
        setTitle("STUB ACTIVITY")

        closeBtn.setOnClickListener { finish() }
    }
}
