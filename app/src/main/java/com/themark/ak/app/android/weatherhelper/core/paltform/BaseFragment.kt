package com.themark.ak.app.android.weatherhelper.core.paltform

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.themark.ak.app.android.weatherhelper.AndroidApplication
import com.themark.ak.app.android.weatherhelper.core.di.ApplicationComponent
import javax.inject.Inject

/**
 * Base Fragment class with helper methods for handling views.
 *
 * @see Fragment
 */
abstract class BaseFragment : Fragment() {

    /**
     * Get a layout id for setting content view.
     */
    protected abstract fun layoutId(): Int

    /**
     * Gets a [ApplicationComponent] for dependency injection.
     */
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as AndroidApplication).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layoutId(), container, false)

    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null
}