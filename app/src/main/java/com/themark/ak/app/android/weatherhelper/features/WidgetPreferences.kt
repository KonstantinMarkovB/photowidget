package com.themark.ak.app.android.weatherhelper.features

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.themark.ak.app.android.weatherhelper.R
import javax.inject.Inject

class WidgetPreferences @Inject constructor(
    private val context: Context,
    private val preferences: SharedPreferences
) {

    fun getUpdateFrequency(id: Int): Int = preferences.getInt(getString(R.string.update_frequency_key) + id, 30)

    fun setUpdateFrequency(id: Int, value: Int) = preferences.edit().putInt(getString(R.string.update_frequency_key) + id, value).apply()

    fun getDataLimit(id: Int): Int = preferences.getInt(getString(R.string.data_limit_key) + id, 1000)

    fun setDataLimit(id: Int, value: Int) = preferences.edit().putInt(getString(R.string.data_limit_key) + id, value).apply()

    fun getText(id: Int): String {
        Log.d("TEST", "getText: $id")
        return preferences.getString(getString(R.string.text_key) + id, "") ?: ""
    }
    fun setText(id: Int, value: String) {
        Log.d("TEST", "set text: $id")
        preferences.edit().putString(getString(R.string.text_key) + id, value).apply()
    }

    private fun getString(id: Int): String = context.resources.getString(id)

}