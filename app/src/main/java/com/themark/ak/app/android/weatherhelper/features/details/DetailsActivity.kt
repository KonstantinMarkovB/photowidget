package com.themark.ak.app.android.weatherhelper.features.details

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.themark.ak.app.android.weatherhelper.R
import com.themark.ak.app.android.weatherhelper.core.exception.Failure
import com.themark.ak.app.android.weatherhelper.core.extention.*
import com.themark.ak.app.android.weatherhelper.core.paltform.BaseActivity
import com.themark.ak.app.android.weatherhelper.core.rx.DisposableObserver
import com.themark.ak.app.android.weatherhelper.core.service.PermissionRationalChecker
import com.themark.ak.app.android.weatherhelper.features.repository.Photo
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoComment
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoDetails
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject


class DetailsActivity : BaseActivity() {
    lateinit var photo: Photo
    lateinit var detailsVM: DetailsViewModel

    var saveButton: MenuItem? = null

    @Inject lateinit var adapter: DetailsAdapter

    private lateinit var permissionChecker: PermissionRationalChecker

    companion object{
        private const val EXTRA_PHOTO = "com.themark.ak.app.android.weatherhelper.imagejouker.EXTRA_PHOTO"

        private val PERMISSIONS = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        fun createIntent(context: Context, photo: Photo): Intent
                = Intent(context, DetailsActivity::class.java).apply {
                putExtra(EXTRA_PHOTO, photo)
            }
    }

    override fun layoutId(): Int = R.layout.activity_details


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        detailsVM = viewMosdel(viewModelFactory){
            observe(photoBitmap, ::handleBitmapLoaded)
            observe(photoDetails, ::handleDetailsLoaded)
            observe(photoComment, ::handleCommentsLoaded)
            observe(photoLoaded, ::handlePhotoLoaded)
            observe(photoSaved, ::handlePhotoSaved)
            failure(failure, ::handleFailure)
        }

        photo = intent.getParcelableExtra(EXTRA_PHOTO)!!

        permissionChecker = PermissionRationalChecker.Builder(this, 1)
            .setPermissions(PERMISSIONS)
            .create()

        initUi()

        detailsVM.load(photo)
    }

    private fun initUi(){
        commentsVM.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        commentsVM.adapter = adapter
    }

    private fun handlePhotoSaved(boolean: Boolean?){
        boolean?.let {
            val mes = if(it) getString(R.string.photo_saved, photo.id)
            else getString(R.string.photo_saved_error)

            Toast.makeText(this, mes, Toast.LENGTH_SHORT).show()
        }
    }

    private fun handleBitmapLoaded(bitmap: Bitmap?) {
        bitmap?.let {
            photoImg.setImageBitmap(it)
        }
    }

    private fun handleDetailsLoaded(photoDetails: PhotoDetails?){
        photoDetails?.let {
            ownerTextView.text = it.username
            titleTextView.text = getString(R.string.photo_title, it.title, it.date)

            if(it.description.trim().isNotEmpty()){
                descriptionTextView.text = it.description
                descriptionTextView.visible()
            } else {
                descriptionTextView.invisible()
            }

            viewsTextView.text = it.views
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.details_menu, menu)
        saveButton = menu?.findItem(R.id.action_save)
        saveButton?.isEnabled = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.action_save -> {
                permissionChecker.checkPermissions(::saveImage)
                true
            }
            R.id.action_full -> {
                openFullImage()
                true
            }
            else -> super.onOptionsItemSelected(item)

        }
    }

    private fun handlePhotoLoaded(isLoaded: Boolean?){
        isLoaded?.let {
            saveButton?.setEnabled(it)
        }
    }

    private fun saveImage(){
        detailsVM.photoBitmap.observe(this, DisposableObserver(detailsVM.photoBitmap){ bmp ->
            if(permissionChecker.hasPermissions()) {
                detailsVM.savePhoto(photo.id)
            } else {
                permissionChecker.request()
            }
        })
    }

    private fun openFullImage() = Intent().apply {
            action = Intent.ACTION_VIEW
            data = Uri.parse("https://www.flickr.com/photos/${photo.owner}/${photo.id}")
            startActivity(this)
        }

    private fun handleCommentsLoaded(photoComments: List<PhotoComment>?){
        photoComments?.let {
            adapter.collection = it
        }
    }

    private fun handleFailure(failure: Failure?){

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(!permissionChecker.handleRequestPermissionsResult(requestCode))
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}