package com.themark.ak.app.android.weatherhelper.core.service

import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.themark.ak.app.android.weatherhelper.R

/**
 * Class for checking and requesting permissions from one group.
 *
 * One group permissions:
 *  __________________________________________________________________________
 * |CALENDAR   |READ_CALENDAR , WRITE_CALENDAR                                |
 * |-----------|--------------------------------------------------------------|
 * |CAMERA     |CAMERA                                                        |
 * |-----------|--------------------------------------------------------------|
 * |CONTACTS   |READ_CONTACTS , WRITE_CONTACTS , GET_ACCOUNTS                 |
 * |-----------|--------------------------------------------------------------|
 * |LOCATION   |ACCESS_FINE_LOCATION , ACCESS_COARSE_LOCATION                 |
 * |-----------|--------------------------------------------------------------|
 * |MICROPHONE |RECORD_AUDIO                                                  |
 * |-----------|--------------------------------------------------------------|
 * |PHONE      |READ_PHONE_STATE, CALL_PHONE, READ_CALL_LOG, WRITE_CALL_LOG,  |
 * |           |ADD_VOICEMAIL , USE_SIP , PROCESS_OUTGOING_CALLS              |
 * |-----------|--------------------------------------------------------------|
 * |SENSORS    |BODY_SENSORS                                                  |
 * |-----------|--------------------------------------------------------------|
 * |SMS        |SEND_SMS, RECEIVE_SMS, READ_SMS, RECEIVE_WAP_PUSH, RECEIVE_MMS|
 * |-----------|--------------------------------------------------------------|
 * |STORAGE    |READ_EXTERNAL_STORAGE , WRITE_EXTERNAL_STORAGE                |
 *  ==========================================================================
 */
abstract class PermissionRationalChecker(val context: Context, open val requestCode: Int) {
    private lateinit var _permissions: Array<String>

    @StringRes private var _rationalTitleId: Int = R.string.permissions_description_title

    @StringRes private var _rationalMessageId: Int = R.string.permissions_description_content

    @StringRes private var _rationalButtonTextId: Int = R.string.ok

    private var method: () -> Any = {}

    fun request(method: () -> Any = {}) {
        this.method = method

        if(shouldShowRequestPermissionRationale(_permissions[0])){
            showRationalDialog(context)
        } else {
            requestPermissions(_permissions)
        }
    }

    fun checkPermissions(method: () -> Any = {}){
        this.method = method
        if(hasPermissions()) method()
        else request(method)
    }

    fun hasPermissions(): Boolean {
        val result = ContextCompat
            .checkSelfPermission(context, _permissions[0])
        return result == PackageManager.PERMISSION_GRANTED
    }

    fun handleRequestPermissionsResult(requestCode: Int): Boolean {
        if(requestCode == requestCode) {
            if(hasPermissions()) method()
            return true
        } else {
            return false
        }
    }

    private fun showRationalDialog(context: Context) {
        MaterialAlertDialogBuilder(context, R.style.DialogTheme)
            .setTitle(_rationalTitleId)
            .setMessage(_rationalMessageId)
            .setPositiveButton(_rationalButtonTextId) { _, _ -> requestPermissions(_permissions) }
            .show()
    }

    protected abstract fun shouldShowRequestPermissionRationale(perm: String): Boolean

    protected abstract fun requestPermissions(perm:  Array<String>)

    private class ActivityChecker(private val activity: AppCompatActivity, context: Context, override val requestCode: Int)
        : PermissionRationalChecker(context, requestCode) {

        override fun shouldShowRequestPermissionRationale(perm: String): Boolean =
            ActivityCompat.shouldShowRequestPermissionRationale(activity, perm)

        override fun requestPermissions(perm: Array<String>) =
            ActivityCompat.requestPermissions(activity, perm, requestCode)
    }

    private class FragmentChecker(private val fragment: Fragment, context: Context, override val requestCode: Int)
        : PermissionRationalChecker(context, requestCode) {

        override fun shouldShowRequestPermissionRationale(perm: String): Boolean =
            fragment.shouldShowRequestPermissionRationale(perm)

        override fun requestPermissions(perm: Array<String>) =
            fragment.requestPermissions(perm, requestCode)
    }

    class Builder {
        private var checker: PermissionRationalChecker

        constructor(fragment: Fragment, requestCode: Int){
            checker = FragmentChecker(fragment, fragment.context!!,  requestCode)
        }
        constructor(activity: AppCompatActivity, requestCode: Int) {
            checker = ActivityChecker(activity, activity, requestCode)
        }

        fun setPermissions(perm: Array<String>): Builder {
            checker._permissions = perm
            return this
        }

        fun setRationaleTitle(@StringRes titleId: Int): Builder {
            checker._rationalTitleId = titleId
            return this
        }

        fun setRationaleMassage(@StringRes messageId: Int): Builder {
            checker._rationalMessageId = messageId
            return this
        }

        fun setRationalPositiveButton(@StringRes textId: Int) : Builder {
            checker._rationalButtonTextId = textId
            return this
        }

        fun create(): PermissionRationalChecker = checker
    }

}