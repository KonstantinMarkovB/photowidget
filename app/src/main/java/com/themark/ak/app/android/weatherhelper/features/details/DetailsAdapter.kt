package com.themark.ak.app.android.weatherhelper.features.details

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.themark.ak.app.android.weatherhelper.R
import com.themark.ak.app.android.weatherhelper.core.extention.inflate
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoComment
import kotlinx.android.synthetic.main.row_comment.view.*
import java.sql.Timestamp
import java.util.*
import javax.inject.Inject
import kotlin.properties.Delegates

class DetailsAdapter
@Inject constructor() : RecyclerView.Adapter<DetailsAdapter.ViewHolder>() {

    var collection: List<PhotoComment> by Delegates.observable(emptyList()){
            _, _, _ -> notifyDataSetChanged()
    }

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(collection[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.row_comment))

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(comment: PhotoComment){
            itemView.userName.text = "${comment.realname} (${comment.realname})"
            itemView.content.text = comment.content
            itemView.date.text = Timestamp(comment.datecreate.toLong()).toString()
        }
    }
}