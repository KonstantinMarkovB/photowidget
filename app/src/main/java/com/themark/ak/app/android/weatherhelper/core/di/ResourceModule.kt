package com.themark.ak.app.android.weatherhelper.core.di

import android.content.Context
import com.themark.ak.app.android.weatherhelper.R
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ResourceModule {

    @Singleton
    @Provides
    @Named(R.string.photo_flickr_key.toString())
    fun photoFlickrKey(context: Context): String =
        context.getString(R.string.photo_flickr_key)

    @Singleton
    @Provides
    @Named(R.string.photo_flickr_user_key.toString())
    fun photoFlickrUserKey(context: Context): String =
        context.getString(R.string.photo_flickr_user_key)

}