package com.themark.ak.app.android.weatherhelper.features.details

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.themark.ak.app.android.weatherhelper.core.paltform.BaseViewModel
import com.themark.ak.app.android.weatherhelper.features.PhotoStorageManager
import com.themark.ak.app.android.weatherhelper.features.repository.Photo
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoComment
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoDetails
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val context: Context,
    private val photoStorageManager: PhotoStorageManager
): BaseViewModel() {

    private val receiver = BroadcastReceiverImpl()

    init {
        val intentFilter = IntentFilter().apply {
            addAction(PhotoService.ACTION_PHOTO_COMMENTS)
            addAction(PhotoService.ACTION_PHOTO_DETAILS)
        }
        val imageFilter = IntentFilter().apply {
            addAction(PhotoService.ACTION_PHOTO_BITMAP)
            addDataType("image/*")
        }
        context.registerReceiver(receiver, intentFilter)
        context.registerReceiver(receiver, imageFilter)
    }

    private val _photoBitmap = MutableLiveData<Bitmap>()
    val photoBitmap: LiveData<Bitmap> = _photoBitmap

    private val _photoComments = MutableLiveData<List<PhotoComment>>()
    val photoComment: LiveData<List<PhotoComment>> = _photoComments

    private val _photoDetails = MutableLiveData<PhotoDetails>()
    val photoDetails: LiveData<PhotoDetails> = _photoDetails

    private val _photoLoaded = MutableLiveData<Boolean>()
    val photoLoaded: LiveData<Boolean> = _photoLoaded

    private val _photoSaved = MutableLiveData<Boolean>()
    val photoSaved: LiveData<Boolean> = _photoSaved


    fun load(photo: Photo){
        context.startService(PhotoService.photoBitmapIntent(context, photo))
        context.startService(PhotoService.photoDetailsIntent(context, photo))
        context.startService(PhotoService.photoCommentIntent(context, photo))
    }

    fun savePhoto(photoName: String){
        _photoBitmap.value?.let { bmp ->
            viewModelScope.launch {
                val res = photoStorageManager.savePublicPhoto(bmp, photoName)

                Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).apply {
                    data = Uri.fromFile(res)
                    context.sendBroadcast(this)
                }

                _photoSaved.value = true
            }

        }
    }

    private inner class BroadcastReceiverImpl: BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(null, "onReceive_________________${intent?.action}")
            if(intent != null){
                when(intent.action){
                    PhotoService.ACTION_PHOTO_DETAILS -> {
                        _photoDetails.value =
                            intent.getParcelableExtra(PhotoService.EXTRA_PHOTO_DETAILS)
                    }
                    PhotoService.ACTION_PHOTO_BITMAP -> {
                        val fileDescriptor = intent.data?.let { uri ->
                            this@DetailsViewModel.context.contentResolver.openFileDescriptor(uri, "r")
                        }
                        _photoBitmap.value = BitmapFactory.decodeFileDescriptor(fileDescriptor?.fileDescriptor)
                        _photoLoaded.value = true
                    }
                    PhotoService.ACTION_PHOTO_COMMENTS -> {
                        _photoComments.value =
                            intent.getParcelableArrayListExtra(PhotoService.EXTRA_PHOTO_COMMENTS)
                    }

                }
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        context.unregisterReceiver(receiver)
    }
}