package com.themark.ak.app.android.weatherhelper.core.extention

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import okhttp3.ResponseBody

val ResponseBody.bitmap: Bitmap
    get() = BitmapFactory.decodeStream(this.byteStream())
