package com.themark.ak.app.android.weatherhelper.features.repository

import android.os.Parcel
import com.themark.ak.app.android.weatherhelper.core.paltform.KParcelable
import com.themark.ak.app.android.weatherhelper.core.paltform.parcelableCreator

data class PhotoComment( val author: String,
                         val authorname: String,
                         val datecreate: String,
                         val realname: String,
                         val content: String): KParcelable{


    companion object {
        @JvmField val CREATOR = parcelableCreator(::PhotoComment)
    }

    constructor(parcel: Parcel) : this (
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeString(author)
            writeString(authorname)
            writeString(datecreate)
            writeString(realname)
            writeString(content)
        }
    }

}