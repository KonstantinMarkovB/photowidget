package com.themark.ak.app.android.weatherhelper.features.repository

/*
    +val username: String,
    +val title: String,
    +val description: String,
    +val date: String,
    val views: String
 */

class PhotoDetailsEntry(
    val photo: PhotoEntry
) {

    companion object {
        fun empty(): PhotoDetailsEntry =
            PhotoDetailsEntry(
                PhotoEntry(
                    OwnerEntry(""),
                    TextEntry(""),
                    TextEntry(""),
                    DatesEntry(""),
                    ""
                )
            )
    }

    fun toPhotoDetails(): PhotoDetails =
        PhotoDetails(
            photo.owner.realname,
            photo.title._content,
            photo.description._content,
            photo.dates.taken,
            photo.views
        )

    class PhotoEntry(
        val owner: OwnerEntry,
        val title: TextEntry,
        val description: TextEntry,
        val dates: DatesEntry,
        val views: String
    )

    class OwnerEntry(
        val realname: String
    )

    class TextEntry(val _content: String)

    class DatesEntry(
        val taken: String
    )
}