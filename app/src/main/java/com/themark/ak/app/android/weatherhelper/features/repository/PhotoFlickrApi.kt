package com.themark.ak.app.android.weatherhelper.features.repository

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoFlickrApi {

    companion object {
        private const val API_KEY = "api_key"
        private const val TEXT = "text"
        private const val PHOTO_ID = "photo_id"
        private const val USER_ID = "user_id"

        private const val FETCH_RECENTS_METHOD = "flickr.photos.getRecent"
        private const val SEARCH_METHOD = "flickr.photos.search"
        private const val COMMENTS_METHOD = "flickr.photos.comments.getList"
        private const val GET_INFO_METHOD = "flickr.photos.getInfo"
        private const val GET_POPULAR_METHOD = "flickr.photos.getPopular"

        private const val URI = "?format=json&nojsoncallback=1&extras=url_s&method="
    }

    @GET(URI + SEARCH_METHOD) fun getPhotos(
        @Query(API_KEY) apiKey: String,
        @Query(TEXT) text: String
    ): Call<PhotosEntry>

    @GET(URI + GET_POPULAR_METHOD) fun getPopulars(
        @Query(API_KEY) apiKey: String,
        @Query(USER_ID) userId: String
    ): Call<PhotosEntry>

    @GET(URI + COMMENTS_METHOD) fun getComments(
        @Query(API_KEY) apiKey: String,
        @Query(PHOTO_ID) photoId: String
    ) : Call<PhotoCommentsEntity>

    @GET(URI + GET_INFO_METHOD) fun getDetails(
        @Query(API_KEY) apiKey: String,
        @Query(PHOTO_ID) photoId: String
    ) : Call<PhotoDetailsEntry>

}