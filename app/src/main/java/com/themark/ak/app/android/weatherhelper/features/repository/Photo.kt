package com.themark.ak.app.android.weatherhelper.features.repository

import android.os.Parcel
import com.themark.ak.app.android.weatherhelper.core.paltform.KParcelable
import com.themark.ak.app.android.weatherhelper.core.paltform.parcelableCreator

data class Photo(
    val title: String,
    val server: String,
    val id: String,
    val secret: String,
    val uri: String,
    val owner: String) : KParcelable{

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Photo)
    }

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeString(title)
            writeString(server)
            writeString(id)
            writeString(secret)
            writeString(uri)
            writeString(owner)
        }
    }
}