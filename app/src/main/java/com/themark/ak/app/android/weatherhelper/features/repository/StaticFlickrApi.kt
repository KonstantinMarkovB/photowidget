package com.themark.ak.app.android.weatherhelper.features.repository

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface StaticFlickrApi {

    companion object{
        const val MOD_SMALL = "_n"
        const val MOD_FULL = ""
    }

    @GET("{server}/{id}_{secret}{mod}.jpg") fun getPhoto(
        @Path("server") server: String,
        @Path("id") id: String,
        @Path("secret") secret: String,
        @Path("mod") mod: String
    ): Call<ResponseBody>
}