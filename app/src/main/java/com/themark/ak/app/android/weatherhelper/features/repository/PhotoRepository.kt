package com.themark.ak.app.android.weatherhelper.features.repository

import android.graphics.Bitmap
import android.util.Log
import com.themark.ak.app.android.weatherhelper.R
import com.themark.ak.app.android.weatherhelper.core.exception.Failure
import com.themark.ak.app.android.weatherhelper.core.exception.Failure.NetworkConnection
import com.themark.ak.app.android.weatherhelper.core.exception.Failure.ServerError
import com.themark.ak.app.android.weatherhelper.core.extention.bitmap
import com.themark.ak.app.android.weatherhelper.core.functional.Either
import com.themark.ak.app.android.weatherhelper.core.functional.Either.Left
import com.themark.ak.app.android.weatherhelper.core.functional.Either.Right
import com.themark.ak.app.android.weatherhelper.core.paltform.NetworkHandler
import retrofit2.Call
import javax.inject.Inject
import javax.inject.Named

interface PhotoRepository {

    fun getPhotos(text: String) : Either<Failure, List<Photo>>

    fun getBitmapSmall(photo: Photo): Either<Failure, Bitmap>

    fun getBitmapFull(photo: Photo): Either<Failure, Bitmap>

    fun getComments(photo: Photo): Either<Failure, List<PhotoComment>>

    fun getDetails(photo: Photo): Either<Failure, PhotoDetails>

    class FlickrRepository @Inject constructor(
        private val networkHandler: NetworkHandler,
        private val service: PhotoFlickrService,
        @Named(R.string.photo_flickr_key.toString()) private val key: String,
        @Named(R.string.photo_flickr_user_key.toString()) private val user_key: String
        ) : PhotoRepository{

        override fun getPhotos(text: String): Either<Failure, List<Photo>>{
            val call = if(text.trim().isEmpty()) service.getPopulars(key, user_key)
                else service.getPhotos(key, text)

            return when (networkHandler.isConnected){
                true -> request(call, { it!!.toPhotos() },PhotosEntry.empty())
                false, null -> Left(NetworkConnection)
            }
        }

        override fun getBitmapSmall(photo: Photo): Either<Failure, Bitmap> =
            getBitmap(photo, StaticFlickrApi.MOD_SMALL)

        override fun getBitmapFull(photo: Photo): Either<Failure, Bitmap> =
            getBitmap(photo, StaticFlickrApi.MOD_FULL)

        private fun getBitmap(photo: Photo, mod: String): Either<Failure, Bitmap> {
            return when (networkHandler.isConnected){
                true -> request(service.getPhoto(photo.server, photo.id, photo.secret, mod),
                    { it?.bitmap ?: Bitmap.createBitmap(25,25,Bitmap.Config.RGB_565)},
                    null
                )
                false, null -> Left(NetworkConnection)
            }
        }

        override fun getComments(photo: Photo): Either<Failure, List<PhotoComment>> {
            return when (networkHandler.isConnected){
                true -> request(service.getComments(key, photo.id),
                    { it!!.toComments() },
                    PhotoCommentsEntity.empty()
                )
                false, null -> Left(NetworkConnection)
            }
        }

        override fun getDetails(photo: Photo): Either<Failure, PhotoDetails> {
            return when (networkHandler.isConnected){
                true -> request(service.getDetails(key, photo.id),
                    { it!!.toPhotoDetails() },
                    PhotoDetailsEntry.empty()
                )
                false, null -> Left(NetworkConnection)
            }
        }

        private fun <T, R> request(call: Call<T>, transform: (T?) -> R, default: T?): Either<Failure, R> {
            return try {
                val response = call.execute()
                when (response.isSuccessful) {
                    true -> Right(transform(response.body() ?: default))
                    false -> Left(ServerError)
                }
            } catch (exception: Throwable) {
                Log.e("TEST", exception.toString())
                Left(ServerError)
            }
        }

    }

}