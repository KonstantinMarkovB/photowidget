package com.themark.ak.app.android.weatherhelper.features.repository

import com.themark.ak.app.android.weatherhelper.core.extention.empty

data class PhotoCommentsEntity(val comments: CommentsEntry) {

    companion object {
        fun empty(): PhotoCommentsEntity =
            PhotoCommentsEntity(CommentsEntry.empty())
    }

    fun toComments(): List<PhotoComment> =
        comments.comment?.map { it.toComment() } ?: emptyList()

    data class CommentsEntry(
        val photo_id: String,
        val comment: List<CommentEntry>?
    ) {
        companion object{
            fun empty() = CommentsEntry(String.empty(), emptyList())
        }
    }

    data class CommentEntry(
        val author: String,
        val authorname: String,
        val datecreate: String,
        val realname: String,
        val _content: String
    ){
        fun toComment() = PhotoComment(
            author,
            authorname,
            datecreate,
            realname,
            _content
        )
    }

}