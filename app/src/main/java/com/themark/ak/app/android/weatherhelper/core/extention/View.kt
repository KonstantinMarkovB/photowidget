package com.themark.ak.app.android.weatherhelper.core.extention

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun View.invisible() { this.visibility = View.GONE }

fun View.visible() { this.visibility = View.VISIBLE }

fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)
