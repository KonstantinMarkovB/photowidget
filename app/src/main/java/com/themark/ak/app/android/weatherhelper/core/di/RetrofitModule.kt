package com.themark.ak.app.android.weatherhelper.core.di

import android.util.Log
import com.themark.ak.app.android.weatherhelper.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class RetrofitModule {
    companion object {
        const val PHOTO_FLICKR = "PHOTO_FLICKR"
        private const val PHOTO_FLICKR_URI = "https://api.flickr.com/services/rest/"

        const val STATICFLICKR = "STATICFLICKR"
        private const val STATICFLICKR_URL = "https://live.staticflickr.com/"
    }

    @Provides
    @Singleton
    @Named(PHOTO_FLICKR)
    fun providePhotoFlickrRetrofit(): Retrofit = provideRetrofit(PHOTO_FLICKR_URI)

    @Provides
    @Singleton
    @Named(STATICFLICKR)
    fun provideStaticFlickrRetrofit(): Retrofit = provideRetrofit(STATICFLICKR_URL)

    private fun provideRetrofit(baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(createClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createClient(): OkHttpClient {
        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG){
            val httpLoggingInterceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Log.d("OkHttp", message)
                }
            })
            val loggingInterceptor =
                httpLoggingInterceptor.apply { level = HttpLoggingInterceptor.Level.BASIC }

            okHttpClientBuilder.addInterceptor(loggingInterceptor)
        }
        return okHttpClientBuilder.build()
    }

}