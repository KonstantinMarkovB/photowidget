package com.themark.ak.app.android.weatherhelper.features.details

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.util.Log
import com.themark.ak.app.android.weatherhelper.AndroidApplication
import com.themark.ak.app.android.weatherhelper.core.exception.Failure
import com.themark.ak.app.android.weatherhelper.features.PhotoStorageManager
import com.themark.ak.app.android.weatherhelper.features.repository.Photo
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoRepository
import javax.inject.Inject

class PhotoService: IntentService("PhotoService") {

    @Inject lateinit var reposypoty: PhotoRepository
    @Inject lateinit var photoStorageManager: PhotoStorageManager

    companion object{
        const val ACTION_PHOTO_DETAILS = "com.themark.ak.app.android.weatherhelper.features.details.ACTION_PHOTO_DETAILS"
        const val EXTRA_PHOTO_DETAILS = "com.themark.ak.app.android.weatherhelper.features.details.PHOTO_DETAILS"

        const val ACTION_PHOTO_BITMAP = "com.themark.ak.app.android.weatherhelper.features.details.ACTION_PHOTO_BITMAP"

        const val ACTION_PHOTO_COMMENTS = "com.themark.ak.app.android.weatherhelper.features.details.ACTION_PHOTO_COMMENTS"
        const val EXTRA_PHOTO_COMMENTS = "com.themark.ak.app.android.weatherhelper.features.details.PHOTO_COMMENTS"

        private const val INTENT_ACTION = "com.themark.ak.app.android.weatherhelper.features.details.INTENT_ACTION"

        private const val EXTRA_PHOTO = "com.themark.ak.app.android.weatherhelper.features.details.PHOTO"

        private const val PHOTO_ID = "TMP"

        fun photoDetailsIntent(context: Context, photo: Photo): Intent =
            Intent(context, PhotoService::class.java).apply {
                putExtra(INTENT_ACTION, ACTION_PHOTO_DETAILS)
                putExtra(EXTRA_PHOTO, photo)
            }
        fun photoBitmapIntent(context: Context, photo: Photo): Intent =
            Intent(context, PhotoService::class.java).apply {
                putExtra(INTENT_ACTION, ACTION_PHOTO_BITMAP)
                putExtra(EXTRA_PHOTO, photo)
            }
        fun photoCommentIntent(context: Context, photo: Photo): Intent =
            Intent(context, PhotoService::class.java).apply {
                putExtra(INTENT_ACTION, ACTION_PHOTO_COMMENTS)
                putExtra(EXTRA_PHOTO, photo)
            }
    }


    override fun onCreate() {
        super.onCreate()
        (application as AndroidApplication).appComponent.inject(this)
        Log.d(null, "Service created")
    }


    override fun onHandleIntent(intent: Intent?) {
        Log.d(null, "On HANDLE")
        if(intent != null){
            val photo: Photo = intent.getParcelableExtra(EXTRA_PHOTO)

            Log.d(null, intent.getStringExtra(INTENT_ACTION))

            when (intent.getStringExtra(INTENT_ACTION)){
                ACTION_PHOTO_DETAILS -> loadDetails(photo)
                ACTION_PHOTO_BITMAP -> loadBitmap(photo)
                ACTION_PHOTO_COMMENTS -> loadComments(photo)
            }
        }
    }

    private fun loadDetails(photo: Photo){
        reposypoty.getDetails(photo).either(::handleFailure){ photoDetails ->
            Intent(ACTION_PHOTO_DETAILS).also {
                it.putExtra(EXTRA_PHOTO_DETAILS, photoDetails)
                sendBroadcast(it)
            }
        }
    }

    private fun loadBitmap(photo: Photo){
        reposypoty.getBitmapFull(photo).either(::handleFailure){ bitmap ->
            Log.d(null, "__________-getBitmapFull___________${bitmap}")
            val uri = photoStorageManager.savePhoto(bitmap, PHOTO_ID)
            Log.d(null, uri.toString())

            Intent(ACTION_PHOTO_BITMAP).also {
//                it.data = uri
                it.setDataAndType(uri, "image/*")
                sendBroadcast(it)
            }
        }
    }

    private fun loadComments(photo: Photo){
        reposypoty.getComments(photo).either(::handleFailure){ comments ->
            Intent(ACTION_PHOTO_COMMENTS).also {
                it.putParcelableArrayListExtra(EXTRA_PHOTO_COMMENTS, ArrayList(comments))
                sendBroadcast(it)
            }
        }
    }

    private fun handleFailure(failure: Failure?){

    }
}