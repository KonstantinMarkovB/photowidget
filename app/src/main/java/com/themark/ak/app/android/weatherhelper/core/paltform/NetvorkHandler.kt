package com.themark.ak.app.android.weatherhelper.core.paltform

import android.content.Context
import com.themark.ak.app.android.weatherhelper.core.extention.networkInfo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkHandler
@Inject constructor(private val context: Context) {
    val isConnected get() = context.networkInfo?.isConnectedOrConnecting
}