package com.themark.ak.app.android.weatherhelper.core.extention

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.themark.ak.app.android.weatherhelper.core.exception.Failure

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) =
    liveData.observe(this, Observer(body))

fun <L : LiveData<Failure>> LifecycleOwner.failure(liveData: L, body: (Failure?) -> Unit) =
    liveData.observe(this, Observer(body))

fun <L : MutableLiveData<Failure>> LifecycleOwner.processFailure(liveData: L, body: (Failure?) -> Boolean) =
    liveData.observe(this, Observer(function = { failure ->
        if(failure != null && body(failure)){
            liveData.value = null
        }
    }))