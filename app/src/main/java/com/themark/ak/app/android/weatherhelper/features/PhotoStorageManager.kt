package com.themark.ak.app.android.weatherhelper.features

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import com.themark.ak.app.android.weatherhelper.BuildConfig
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class PhotoStorageManager @Inject constructor(
    private val context: Context
) {

    companion object {
        private const val AUTHORITY = "${BuildConfig.APPLICATION_ID}.fileprovider"
        private const val PHOTOS_DIR = "shared_images"
    }


    fun savePhoto(bitmap: Bitmap, id: String): Uri {

        val imageDir = getFileDir()

        val image = File(imageDir,getFileName(id))
        image.createNewFile()

        FileOutputStream(image).use {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
        }

        return FileProvider.getUriForFile(context, AUTHORITY, image)
    }

    fun savePublicPhoto(bitmap: Bitmap, name:String): File {
        val file = getPublicFileDir("$name.png")
        FileOutputStream(file).use {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
        }

        return file
    }

    fun delete(id: String): Boolean = File(getFileDir(), getFileName(id)).delete()


    fun clear(){
        getFileDir().listFiles().forEach {
            it.delete()
        }
    }

    private fun getFileDir() : File {
        val fileDir = File(context.getExternalFilesDir(null), PHOTOS_DIR)
        fileDir.mkdir()
        return fileDir
    }

    private fun getPublicFileDir(fileName: String): File = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            fileName
        )

    private fun getFileName(id: String) = "img$id.png"

}