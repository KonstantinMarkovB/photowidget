package com.themark.ak.app.android.weatherhelper.core.paltform

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.themark.ak.app.android.weatherhelper.AndroidApplication
import com.themark.ak.app.android.weatherhelper.R
import com.themark.ak.app.android.weatherhelper.core.di.ApplicationComponent
import javax.inject.Inject

/**
 * Base Activity class with helper methods for handling fragment transaction.
 *
 * @see AppCompatActivity
 */
abstract class BaseActivity : AppCompatActivity() {

    /**
     * Get a layout id for setting content view.
     */
    abstract protected open fun layoutId(): Int

    protected open fun optionsMenuId() : Int? = null

    /**
     * Gets a [ApplicationComponent] for dependency injection.
     */
    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (this.application as AndroidApplication).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    /**
     * Change current app theme from LauncherTheme to AppTheme.
     *
     * The LauncherTheme contains background screen to display
     * during the app launching.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        optionsMenuId()?.let {
            menuInflater.inflate(it, menu)
            return true
        }
        return false
    }

    /**
     * Adds a [Fragment] to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment The fragment to be added.
     */
    protected fun addFragment(containerViewId: Int, fragment: Fragment) {
        this.supportFragmentManager.beginTransaction()
            .add(containerViewId, fragment).commit()
    }

    /**
     * Show a dialog fragment.
     */
    protected fun showDialogFragment(dialogFragment: DialogFragment){
        val ft = this.supportFragmentManager.beginTransaction()
        val prev = this.supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)
        dialogFragment.show(ft, "dialog")
    }

    protected fun isPermissionGranted(permission: String) =
        (ContextCompat.checkSelfPermission(this, permission)
                == PackageManager.PERMISSION_GRANTED)

    protected fun sendPermissionsRequest(requestCode: Int, vararg permission: String){
        ActivityCompat.requestPermissions(this,
            permission,
            requestCode)
    }

    protected fun isGranted(grantResults: IntArray) = grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED

    protected fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and request.
                        or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    protected fun showSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }
}