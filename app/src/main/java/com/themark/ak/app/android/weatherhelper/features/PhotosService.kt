package com.themark.ak.app.android.weatherhelper.features

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.*
import android.util.Log
import android.util.SparseArray
import com.themark.ak.app.android.weatherhelper.AndroidApplication
import com.themark.ak.app.android.weatherhelper.core.exception.Failure
import com.themark.ak.app.android.weatherhelper.features.repository.Photo
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoRepository
import java.util.*
import javax.inject.Inject



class PhotosService : Service() {

    @Inject lateinit var preferences: WidgetPreferences
    @Inject lateinit var repository: PhotoRepository
    @Inject lateinit var mPhotoManager: PhotoStorageManager

    private var serviceLooper: Looper? = null
    private var serviceHandler: ServiceHandler? = null

    private val photosMap = SparseArray<Stack<Photo>>()
    private val textMap = SparseArray<String>()

    companion object{
        const val EXTRA_PHOTO_RESULT = "com.themark.ak.app.android.weatherhelper.EXTRA_PHOTO_RESULT"

        private const val PENDING_INTENT = "com.themark.ak.app.android.weatherhelper.PENDING"
        private const val BUNDLE_EXTRAS = "com.themark.ak.app.android.weatherhelper.BUNDLE_EXTRAS"
        private const val ACTION_EXTRAS = "com.themark.ak.app.android.weatherhelper.ACTION_EXTRAS"
        private const val ACTION_EXTRA_PERF_ID = "com.themark.ak.app.android.weatherhelper.ACTION_EXTRA_PERF_ID"

        fun getIntent(context: Context, clazz: Class<*>, action: String, extra: Bundle, prefId: Int): Intent =
            Intent(context, PhotosService::class.java).apply {
                putExtra(PENDING_INTENT, clazz.name)
                putExtra(BUNDLE_EXTRAS, extra)
                putExtra(ACTION_EXTRAS, action)
                putExtra(ACTION_EXTRA_PERF_ID, prefId)
            }

        fun stopService(context: Context) = Intent(context, PhotosService::class.java)
    }

    private inner class ServiceHandler(looper: Looper) : Handler(looper) {

        override fun handleMessage(msg: Message) {
            val intent: Intent? = msg.obj as Intent
            intent?.let { onHandleIntent(intent) }
        }
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(null, "service onCreate")
        (application as AndroidApplication).appComponent.inject(this)

        HandlerThread("PhotosService", Process.THREAD_PRIORITY_BACKGROUND).apply {
            start()

            serviceLooper = looper
            serviceHandler = ServiceHandler(looper)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        serviceHandler?.obtainMessage()?.also { msq ->
            msq.arg1 = startId
            msq.obj = intent

            serviceHandler?.sendMessage(msq)
        }

        return START_STICKY
    }

    fun onHandleIntent(p0: Intent?) {
        p0?.let { intent ->
            val id = intent.getIntExtra(ACTION_EXTRA_PERF_ID, 0)
            val text = preferences.getText(id)

            val photos: Stack<Photo> = getPhotos(id, text)

            if(photos.isEmpty()){
                Log.d(null, "getPhotos ${photos.size}")

                repository.getPhotos(text).either(::handleFailure){
                    Log.d(null, "getPhotos ok ${photos.size}")
                    photos.addAll(it)
                    loadPhoto(photos.pop(), intent)
                }
            } else {
                loadPhoto(photos.pop(), intent)
            }
        }
    }

    private fun getPhotos(id: Int, text: String): Stack<Photo>{
        val oldText = textMap[id]

        if(text != oldText){
            textMap.put(id, text)
            photosMap[id]?.clear()
        }

        if(photosMap[id] == null) photosMap.put(id, Stack())


        return photosMap[id]
    }

    private fun loadPhoto(photo: Photo, intent: Intent){
        Log.d(null, "loadPhoto")
        val id = intent.getIntExtra(ACTION_EXTRA_PERF_ID, 0)


        repository.getBitmapSmall(photo).either(::handleFailure){ bitmap ->
            Log.d(null, "${bitmap.byteCount}")

            val action = intent.getStringExtra(ACTION_EXTRAS)

            val className = intent.getStringExtra(PENDING_INTENT)
            val clazz = Class.forName(className)

            val transExtra = intent.getBundleExtra(BUNDLE_EXTRAS)

            val bitmapUri = mPhotoManager.savePhoto(bitmap, id.toString())

            val resultIntent = Intent(baseContext, clazz).apply {
                this.action = action
                putExtra(EXTRA_PHOTO_RESULT, photo)
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                putExtras(transExtra)
                setDataAndType(bitmapUri, "image/*")
            }

            sendBroadcast(resultIntent)
        }
    }

    private fun handleFailure(failure: Failure?){
        Log.d(null, "$failure")
    }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onDestroy() {
        super.onDestroy()
        photosMap.clear()
        mPhotoManager.clear()
        Log.d(null, "service onDestroy")
    }
}