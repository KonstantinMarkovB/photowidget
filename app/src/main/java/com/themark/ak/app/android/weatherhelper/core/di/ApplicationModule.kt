package com.themark.ak.app.android.weatherhelper.core.di

import android.content.Context
import android.content.SharedPreferences
import com.themark.ak.app.android.weatherhelper.AndroidApplication
import com.themark.ak.app.android.weatherhelper.R
import com.themark.ak.app.android.weatherhelper.features.repository.PhotoRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    fun providePhotoRepositoryContext(ropository: PhotoRepository.FlickrRepository): PhotoRepository =
        ropository

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(context.resources.getString(R.string.preference_file_key),0)
}