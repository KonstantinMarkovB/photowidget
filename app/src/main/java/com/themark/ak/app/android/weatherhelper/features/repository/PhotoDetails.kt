package com.themark.ak.app.android.weatherhelper.features.repository

import android.os.Parcel
import com.themark.ak.app.android.weatherhelper.core.paltform.KParcelable
import com.themark.ak.app.android.weatherhelper.core.paltform.parcelableCreator

data class PhotoDetails(
    val username: String,
    val title: String,
    val description: String,
    val date: String,
    val views: String
) : KParcelable{
    companion object {
        @JvmField val CREATOR = parcelableCreator(::PhotoDetails)
    }

    constructor(parcel: Parcel) : this (
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeString(username)
            writeString(title)
            writeString(description)
            writeString(date)
            writeString(views)
        }
    }
}