package com.themark.ak.app.android.weatherhelper.core.di

import com.themark.ak.app.android.livebook.core.di.viewmodel.ViewModelModule
import com.themark.ak.app.android.weatherhelper.AndroidApplication
import com.themark.ak.app.android.weatherhelper.features.ConfigureActivity
import com.themark.ak.app.android.weatherhelper.features.PhotosService
import com.themark.ak.app.android.weatherhelper.features.details.DetailsActivity
import com.themark.ak.app.android.weatherhelper.features.details.DetailsViewModel
import com.themark.ak.app.android.weatherhelper.features.details.PhotoService
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    RetrofitModule::class,
    ResourceModule::class,
    ViewModelModule::class
])
interface ApplicationComponent {
    fun inject(application: AndroidApplication)
    fun inject(configureActivity: ConfigureActivity)
    fun inject(photosService: PhotosService)
    fun inject(detailsActivity: DetailsActivity)
    fun inject(detailsViewModel: DetailsViewModel)
    fun inject(photoService: PhotoService)
}