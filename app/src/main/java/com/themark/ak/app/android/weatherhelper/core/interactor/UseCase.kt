package com.themark.ak.app.android.weatherhelper.core.interactor

import com.themark.ak.app.android.weatherhelper.core.exception.Failure
import com.themark.ak.app.android.weatherhelper.core.functional.Either
import kotlinx.coroutines.GlobalScope

import kotlinx.coroutines.*

/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This abstraction represents an execution unit for different use cases (this means than any use
 * case in the application should implement this contract).
 *
 * By convention each [UseCase] implementation will execute its job in a background thread
 * (kotlin coroutine) and will post the result in the UI thread.
 */
abstract class UseCase<out Type, in Params> where Type : Any {

    abstract suspend fun run(params: Params): Either<Failure, Type>

    var scope: CoroutineScope = GlobalScope

    operator fun invoke(
        params: Params,
        onResult: (Either<Failure, Type>) -> Unit = {}
    ) {
        val job = GlobalScope.async { run(params) }
        scope.launch(Dispatchers.Main) { onResult(job.await())  }
    }

    class None
}
