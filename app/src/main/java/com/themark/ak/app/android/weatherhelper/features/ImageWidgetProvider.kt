package com.themark.ak.app.android.weatherhelper.features

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.provider.MediaStore
import android.util.Log
import android.widget.RemoteViews
import com.themark.ak.app.android.weatherhelper.R
import com.themark.ak.app.android.weatherhelper.features.details.DetailsActivity
import com.themark.ak.app.android.weatherhelper.features.repository.Photo

class ImageWidgetProvider : AppWidgetProvider() {

    companion object {
        const val ACTION_UPDATE_IMAGE = "com.themark.ak.app.android.weatherhelper.ACTION_UPDATE_IMAGE"

        fun refresh(context: Context, appWidgetId: Int) =
            context.startService(updateImageIntent(context, appWidgetId))

        fun updateImageIntent(context: Context, appWidgetId: Int): Intent{
            val extra = Bundle().apply {
                putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            }

            return PhotosService.getIntent(
                context, ImageWidgetProvider::class.java, ACTION_UPDATE_IMAGE, extra, appWidgetId
            ).apply {
                data = Uri.parse(appWidgetId.toString())
            }
        }

        private fun update(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
            Log.d(null, "update, id=$appWidgetId")

            val pendingUpdate = PendingIntent.getService(
                context, 0, updateImageIntent(context, appWidgetId), 0)

            val configIntent = ConfigureActivity.intent(context, appWidgetId).apply {
                data = Uri.parse(appWidgetId.toString())
            }
            val pendingConfig = PendingIntent.getActivity(
                context, 0, configIntent, 0
            )

            RemoteViews(context.packageName, R.layout.widget_layout).apply {
                setOnClickPendingIntent(R.id.actionUpdate, pendingUpdate)
                setOnClickPendingIntent(R.id.actionConfig, pendingConfig)

                appWidgetManager.updateAppWidget(appWidgetId, this)
            }

            Log.d(null, "update ok, id=$appWidgetId")
        }
    }

    override fun onUpdate(context: Context?, appWidgetManager: AppWidgetManager?, appWidgetIds: IntArray?) {

        Log.d(null, "onUpdate")
        if (appWidgetIds != null && context!= null && appWidgetManager!= null) {
            appWidgetIds.forEach { appWidgetId ->
                update(context, appWidgetManager, appWidgetId)
            }
        }
    }


    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("TEST", "ON RESIVE________  ${intent?.action}")
        if(context != null && intent != null){
            when(intent.action){

                ACTION_UPDATE_IMAGE -> {
                    updateImage(context, intent)
                }
            }
        }

        super.onReceive(context, intent)
    }

    private fun updateImage(context: Context, intent: Intent){
        val appWidgetManager = AppWidgetManager.getInstance(context)

        val appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID)

        val photo: Photo? = intent.getParcelableExtra(PhotosService.EXTRA_PHOTO_RESULT)

        val fileDescriptor = intent.data?.let { uri ->
            context.contentResolver.openFileDescriptor(uri, "r")
        }
        val bitmap: Bitmap? = BitmapFactory.decodeFileDescriptor(fileDescriptor?.fileDescriptor)

        val detailsIntent = DetailsActivity.createIntent(context, photo!!).apply {
            data = Uri.parse(appWidgetId.toString())
        }
        val pendingDetails = PendingIntent.getActivity(
            context, 0, detailsIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        RemoteViews(context.packageName, R.layout.widget_layout).apply {
            setImageViewBitmap(R.id.imageView, bitmap)

            setOnClickPendingIntent(R.id.imageView, pendingDetails)

            appWidgetManager.updateAppWidget(appWidgetId, this)
        }
    }

    override fun onDeleted(context: Context?, appWidgetIds: IntArray?) {
        if(context!= null && appWidgetIds != null){
            appWidgetIds.forEach {
                ConfigureActivity.closeWidget(context, it)
            }
        }
        super.onDeleted(context, appWidgetIds)
    }

    override fun onDisabled(context: Context?) {
        super.onDisabled(context)
        context?.apply {
            Log.d(null, "Prowider onDisabled")
            stopService(PhotosService.stopService(context))
        }
    }
}