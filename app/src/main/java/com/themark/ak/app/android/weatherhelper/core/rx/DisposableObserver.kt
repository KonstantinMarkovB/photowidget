package com.themark.ak.app.android.weatherhelper.core.rx

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

class DisposableObserver <T> (private val observed: LiveData<T>, private val callback: (T)->Unit) : Observer<T> {

    init {
        observed.observeForever(this)
    }

    override fun onChanged(t: T) {
        t?.let {
            observed.removeObserver(this)
            callback(t)
        }
    }
}