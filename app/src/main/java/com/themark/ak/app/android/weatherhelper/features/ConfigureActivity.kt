package com.themark.ak.app.android.weatherhelper.features

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.themark.ak.app.android.weatherhelper.AndroidApplication
import com.themark.ak.app.android.weatherhelper.R
import com.themark.ak.app.android.weatherhelper.StubActivity
import kotlinx.android.synthetic.main.activity_widget_configuration.*
import kotlinx.android.synthetic.main.row_comment.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.properties.Delegates


class ConfigureActivity : AppCompatActivity() {

    private val updateFrequencyFrom = 30
    private val updateFrequencyTo = 1144

    private val dataLimitFrom = 1
    private val dataLimitTo = 1000

    @Inject lateinit var preferences: WidgetPreferences

    private var mAppWidgetId by Delegates.notNull<Int>()
    private var mFirsTimeCalled by Delegates.notNull<Boolean>()

    companion object {
        private const val FIRS_TIME_CALLED = "com.themark.ak.app.android.weatherhelper.features.FIRS_TIME_CALLED"

        fun intent(context: Context, id:Int) =
            Intent(context, ConfigureActivity::class.java).apply {
                putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id)
                putExtra(FIRS_TIME_CALLED, false)
                data = Uri.parse(toUri(Intent.URI_INTENT_SCHEME))
            }

        fun closeWidget(context: Context, appWidgetId: Int){
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pendingIntent = createPendingIntent(context, appWidgetId)
            alarmManager.cancel(pendingIntent)
            pendingIntent.cancel()
        }

        private fun createPendingIntent(context: Context, appWidgetId: Int): PendingIntent{
            val intent = ImageWidgetProvider.updateImageIntent(context, appWidgetId)
            return PendingIntent.getService(context, 0, intent, 0)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as AndroidApplication).appComponent.inject(this)

        setContentView(R.layout.activity_widget_configuration)

        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
            AppWidgetManager.INVALID_APPWIDGET_ID)

        mFirsTimeCalled = intent.getBooleanExtra(FIRS_TIME_CALLED, true)

        setResult(RESULT_CANCELED)

        initUi()
    }

    private fun initUi(){
        actionConfirm.setOnClickListener { confirmAction() }
        actionCancel.setOnClickListener { finish() }

        pickerUpdateFrequency.minValue = updateFrequencyFrom
        pickerUpdateFrequency.maxValue = updateFrequencyTo
        pickerUpdateFrequency.value = preferences.getUpdateFrequency(mAppWidgetId)

        pickerDataLimit.minValue = dataLimitFrom
        pickerDataLimit.maxValue = dataLimitTo
        pickerDataLimit.value = preferences.getDataLimit(mAppWidgetId)

        text.setText(preferences.getText(mAppWidgetId).trim())

        text.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                p0?.let {
                    if(it.contains("\n")){
                        text.setText(it.subSequence(0, it.lastIndex))
                        hideKeyBoard()
                    }
                }
            }

        })
    }

    private fun confirmAction(){
        val newUpdateFrequency = pickerUpdateFrequency.value
        val newDataLimit = pickerDataLimit.value

        preferences.setUpdateFrequency(mAppWidgetId, newUpdateFrequency)
        preferences.setDataLimit(mAppWidgetId, newDataLimit)
        preferences.setText(mAppWidgetId, text.text.toString().trim())

        setAlarm(newUpdateFrequency)

        if(mFirsTimeCalled){
            val resultValue = Intent().apply {
                putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId)
            }

            setResult(Activity.RESULT_OK, resultValue)
        }

        ImageWidgetProvider.refresh(baseContext, mAppWidgetId)

        finish()
    }

    private fun hideKeyBoard(){
        val imm = baseContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(text.windowToken, 0)
    }

    private fun setAlarm(interval: Int){
        val milis = TimeUnit.MINUTES.toMillis(interval.toLong())

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val pendingIntent = createPendingIntent(this, mAppWidgetId)

        if(!mFirsTimeCalled) alarmManager.cancel(pendingIntent)

        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), milis, pendingIntent)
    }

}