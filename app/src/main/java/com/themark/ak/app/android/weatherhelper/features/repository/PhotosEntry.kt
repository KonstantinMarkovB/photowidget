package com.themark.ak.app.android.weatherhelper.features.repository

import com.themark.ak.app.android.weatherhelper.core.extention.empty

data class PhotosEntry(val photos: PhotosEntry) {

    companion object {
        fun empty() = PhotosEntry(PhotosEntry.empty())
    }

    fun toPhotos(): List<Photo> =
        photos.photo.mapNotNull {
            if (it.url_s != null) it.toPhoto() else null
        }


    data class PhotosEntry(
        val page: Int,
        val pages: Int,
        val perpage: Int,
        val total: Int,
        val photo: List<PhotoEntry>
    ) {
        companion object{
            fun empty(): PhotosEntry =
                PhotosEntry(0,0,0,0, emptyList())
        }
    }

    data class PhotoEntry(
        val id: String,
        val owner: String,
        val secret: String,
        val server: String,
        val farm: Int,
        val title: String,
        val ispublic: Int,
        val isfriend: Int,
        val isfamily: Int,
        val url_s: String?,
        val height_s: String,
        val width_s: String
    ) {
        fun toPhoto(): Photo =
            Photo(
                title,
                server,
                id,
                secret,
                url_s ?: String.empty(),
                owner
            )
    }
}