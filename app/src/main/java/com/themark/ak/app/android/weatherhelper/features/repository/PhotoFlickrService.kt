package com.themark.ak.app.android.weatherhelper.features.repository

import com.themark.ak.app.android.weatherhelper.core.di.RetrofitModule
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class PhotoFlickrService @Inject constructor(
    @Named(RetrofitModule.PHOTO_FLICKR) retrofitPF: Retrofit,
    @Named(RetrofitModule.STATICFLICKR) retrofitST: Retrofit
) : PhotoFlickrApi, StaticFlickrApi {

    private val photoFlickrApi by lazy { retrofitPF.create(PhotoFlickrApi::class.java) }
    private val staticFlickrApi by lazy { retrofitST.create(StaticFlickrApi::class.java) }

    override fun getPhotos(apiKey: String, text: String): Call<PhotosEntry> =
        photoFlickrApi.getPhotos(apiKey, text)

    override fun getPopulars(apiKey: String, userId: String): Call<PhotosEntry> =
        photoFlickrApi.getPhotos(apiKey, userId)

    override fun getPhoto(server: String, id: String, secret: String, mod: String): Call<ResponseBody> =
        staticFlickrApi.getPhoto(server, id, secret, mod)

    override fun getComments(apiKey: String, photoId: String): Call<PhotoCommentsEntity> =
        photoFlickrApi.getComments(apiKey, photoId)

    override fun getDetails(apiKey: String, photoId: String): Call<PhotoDetailsEntry> =
        photoFlickrApi.getDetails(apiKey, photoId)
}